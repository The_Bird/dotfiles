# mutt a shit
COLORFGBG="default;default"

alias la='ls -lAFh --color=auto'
alias dir="dir --color=auto"
alias grep="grep --color=auto"

alias cp='cp -i'
alias mv='mv -i'
alias ln='ln -i'
alias cd..='cd ..'
alias vi='vim'

alias wmutt="urxvt -name mutt -title mutt -e mutt"
alias desktop="clear && echo && echo && screenfetch -s && mv -f screenFetch* desktop.png"
alias gimme='sudo chmod -R 755 * && sudo chown -R thebird * && sudo chgrp -R thebird *'
alias http="python -m SimpleHTTPServer"
alias dropcaches="sudo bash -c 'echo 1 > /proc/sys/vm/drop_caches'"
alias ytaudio="youtube-dl -x --audio-format vorbis --audio-quality 0 -o '%(title)s.%(ext)s'"
alias lwget="wget --user=thebird --password=mudkip"

export PS1="\[\033[32m\]\u@\h\[\033[34m\] \w $([[ $? != 0 ]] && echo "\[\033[01;31m\]\[\033[01;34m\]")\$\[\033[00m\] "
export PROMPT_COMMAND="echo -ne '\a'"
export PATH=$PATH:~/bin
