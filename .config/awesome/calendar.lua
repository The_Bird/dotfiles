local calendar = { mt = {} }

function calendar.new(args)
    local args                 = args                  or {}
    local hilight_bg_color     = args.hilight_bg_color or "#444444"
    local hilight_fg_color     = args.hilight_fg_color or "#90b3b3"
    local height               = args.height           or 8
    local sed                  = args.sed              or "sed s/'  $'//g | sed \"/^ *$/d\" "
    local settings             = args.settings         or function () end
    calendar.month             = tonumber(os.date("%m"))
    calendar.year              = tonumber(os.date("%Y"))
    calendar.notification      = nil
    calendar.notification_args = { timeout  = 0,
                                   position = "bottom_right" }
        
    function calendar.update(month_inc)
        calendar.month = calendar.month + (month_inc or 0)
        if calendar.month > 12 then calendar.month = 1 ; calendar.year = calendar.year + 1 end
        if calendar.month < 1  then calendar.month = 12; calendar.year = calendar.year - 1 end
        calendar.today = tonumber(os.date('%d'))
        calendar.notification_args.text = ""
        nlines                          = 0
        for i in io.popen("cal -h -m " .. calendar.month .. " " .. calendar.year .. " | " .. sed):lines() do
            calendar.notification_args.text = calendar.notification_args.text .. i .. "\n"
            nlines                          = nlines + 1
        end
        for i = 1,(8-nlines) do
            if i == 1 then calendar.notification_args.text = calendar.notification_args.text:gsub("\nSu","\n\nSu") end
            if i == 2 then calendar.notification_args.text = calendar.notification_args.text:gsub("28","28\n")     end
        end
        calendar.notification_args.text = calendar.notification_args.text:gsub("\n$","")
        month_inc = 0
        settings()
    end
    calendar.update()
    function calendar.display()
        calendar.notification = naughty.notify(calendar.notification_args)
    end
end

function calendar.mt:__call(...)
    return calendar.new(...)
end

return setmetatable(calendar,calendar.mt)
