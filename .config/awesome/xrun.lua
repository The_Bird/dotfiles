xrun = function(cmd, late)
    local xresources = awful.util.pread("xrdb -query")
    local myxr = xresources:match("^awesome%.xrun:%s+.-\n")
                 or xresources:match("\nawesome%.xrun:%s+.-\n")
                 or "awesome.xrun:"
    myxr = myxr:gsub("\n", "")
    if not string.find(myxr, cmd, 1, true) then
        -- use pread so that this runs synchronously
        awful.util.pread("echo '" .. myxr .. " " .. cmd:gsub("'", "\\'") .. "'|xrdb -merge")
        if late then
            local stimer = timer { timeout = late }
            local run = function()
                stimer:stop()
                awful.util.spawn(cmd, false)
            end
            stimer:connect_signal("timeout", run)
            stimer:start()
        else
            awful.util.spawn(cmd, false)
        end
    end
end
