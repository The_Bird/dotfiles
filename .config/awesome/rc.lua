
require("includes")

require("error")
require("variables")
require("tags")
require("widgets")
require("bar")
require("keys")
require("rules")
require("signals")
require("xrun")

require("autorun")
