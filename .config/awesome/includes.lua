-- Standard awesome library
gears = require("gears")
awful = require("awful")
awful.rules = require("awful.rules")
autofocus = require("awful.autofocus")

-- Widget and layout library
wibox = require("wibox")
vicious = require("vicious")

-- Theme handling library
beautiful = require("beautiful")

-- Notification library
naughty = require("naughty")
menubar = require("menubar")

-- Lain
lain = require("lain")
helpers = require("lain.helpers")
markup = lain.util.markup
