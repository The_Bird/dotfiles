-- {{{ Wibox
local newprompt = require("newprompt")

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
											  client.focus = c
											  c:raise()
                                          end))
for s = 1, screen.count() do
    -- Create a promptbox for each screen
    --~ mypromptbox[s] = newprompt({ prompt = "<span fgcolor=\"#826ade\">thebird@thebird</span> <span fgcolor=\"#4e84d5\">~ $</span> " })
    mypromptbox[s] = newprompt({ prompt = '<span fgcolor="#4e84d5">~ $</span> ' })
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
		--mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)
		-- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)
	
    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })

    -- Widgets that are aligned to the left
    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(linel)
    left_layout:add(mytaglist[s])
    left_layout:add(liner)
    left_layout:add(promptl)
    left_layout:add(promptspace)
    left_layout:add(mypromptbox[s])
    left_layout:add(promptspace)
    left_layout:add(promptr)

    -- Widgets that are aligned to the center
    local center_layout = wibox.layout.flex.horizontal()
    center_layout:add(bg)

    -- Widgets that are aligned to the right
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(linel)
    right_layout:add(mailspace)
    right_layout:add(mailicon)
    right_layout:add(mailwidget)
    right_layout:add(mailspace)
    right_layout:add(maill)
    right_layout:add(mailr)
    right_layout:add(space)
    right_layout:add(batteryicon)
    right_layout:add(battery)
    right_layout:add(space)
    right_layout:add(cpuicon)
    right_layout:add(cpu)
    right_layout:add(space)
    right_layout:add(memicon)
    right_layout:add(mem)
    right_layout:add(space)
    right_layout:add(wifiicon)
    right_layout:add(wifi)
    right_layout:add(space)
    right_layout:add(volumeicon)
    right_layout:add(volume)
    right_layout:add(space)
    right_layout:add(thermicon)
    right_layout:add(therm)
    right_layout:add(space)
    right_layout:add(weathericon)
    right_layout:add(weatherwidget)
    right_layout:add(weatherspace)
    right_layout:add(liner)
    right_layout:add(linel)
    right_layout:add(space)
    right_layout:add(mpdwidget)
    right_layout:add(space)
    right_layout:add(liner)
    right_layout:add(linel)
    right_layout:add(space)
    right_layout:add(textclock)
    right_layout:add(space)
    right_layout:add(liner)
    right_layout:add(linel)
    right_layout:add(space)
    right_layout:add(mylayoutbox[s])
    right_layout:add(space)
    right_layout:add(wibox.widget.systray())
    right_layout:add(liner)
    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    --layout:set_middle(mytasklist[s])
    layout:set_middle(center_layout)
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)
end
-- }}}
