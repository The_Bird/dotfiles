-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "mpv" },
      properties = { floating = true } },
    { rule = { instance = "vlc" },
      properties = { floating = true } },
    { rule = { instance = "Toplevel" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "Viewnior" },
      properties = { floating = true } },
    { rule = { instance = "fterminal" },
      properties = { floating = true } },
    { rule = { instance = "fontforge" },
      properties = { floating = true } },
    { rule = { class = "feh" },
      properties = { floating = true }, callback = function (c) awful.placement.centered(c,nil) end },
    { rule = { class = "Firefox" },
      properties = { tag = tags[1][2] } },
    { rule = { instance = "thunar" },
      properties = { tag = tags[1][3] } },
    { rule = { instance = "geany" },
      properties = { tag = tags[1][4] } },
    { rule = { class = "libreoffice" },
      properties = { tag = tags[1][4] } },
    { rule = { class = "VirtualBox" },
      properties = {floating = true, tag = tags[1][5] } },
    { rule = { instance = "mame" },
      properties = { tag = tags[1][5] } },
    { rule = { instance = "gimp" },
      properties = { tag = tags[1][6] } },
    { rule = { instance = "krita" },
      properties = { tag = tags[1][6] } },
    { rule = { instance = "ncmpcpp" },
      properties = { tag = tags[1][1], focus = 1, x = 7, y = 105, width = 473, height = 220} },
    { rule = { instance = "htop" },
      properties = { tag = tags[1][1], focus = 1, x = 7, y = 32, width = 473, height = 63} },
    { rule = { instance = "mutt" },
      properties = { tag = tags[1][1], focus = 1, x = 7, y = 335, width = 473, height = 220} },
    { rule = { instance = "startterm" },
      properties = { tag = tags[1][1], focus = 1, x = 490, y = 32, width = 571, height = 293} },
    { rule = { instance = "terminal" },
      properties = { focus = 1, width = 550, height = 316 }, callback = function (c) awful.placement.centered(c,nil) end },
}
-- }}}
