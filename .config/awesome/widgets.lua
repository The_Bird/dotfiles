-- {{{ Widgets

weather  = require("weather")
mpd      = require("mpd")
mail     = require("mail")
calendar = require("calendar")

function purgenotifications()
    if weather then naughty.destroy(weather.notification) end
    if mpd     then naughty.destroy(mpd.notification)     end
end

naughty.config.padding = 9

-- Seperators
promptl     = wibox.widget.imagebox("")
promptspace = wibox.widget.imagebox("")
promptr     = wibox.widget.imagebox("")
bg          = wibox.widget.background(wibox.widget.textbox(""), "#444444")
line        = wibox.widget.imagebox(beautiful.seperator)
linel       = wibox.widget.imagebox(beautiful.seperator_left)
liner       = wibox.widget.imagebox(beautiful.seperator_right)
space       = wibox.widget.imagebox(beautiful.space)
greyspace   = wibox.widget.imagebox(beautiful.greyspace)
blackgrey   = wibox.widget.imagebox(beautiful.blackgrey)
greyblack   = wibox.widget.imagebox(beautiful.greyblack)

-- Clock
calendar({settings = function ()
        hilight_fg_color = "#90b3b3"
        hilight_bg_color = "#444444"
        _ = ""
        if calendar.today < 10 then
            _ = " "
        end
        if calendar.notification_args.text:match(_ .. calendar.today .. "\n")
        or calendar.notification_args.text:match("\n ?" .. _ .. calendar.today) then
            hilight_fg_color = "#a984cf"
        end
        local hilight_replacement = "%1<span fgcolor='" .. hilight_fg_color ..
                                    "' bgcolor='"       .. hilight_bg_color ..
                                    "' >%2</span>%3"
        if calendar.today < 10 then
            hilight_replacement = "<span fgcolor='"     .. hilight_fg_color ..
                                  "' bgcolor='"         .. hilight_bg_color ..
                                  "' >%1%2</span>%3"
        end
        if calendar.month ~= tonumber(os.date('%m')) or
           calendar.year  ~= tonumber(os.date('%Y')) then
            hilight_replacement = "%1%2%3"
        end
        calendar.notification_args.text = calendar.notification_args.text:gsub("(%s)(" .. calendar.today  .. ")(%s)", hilight_replacement)
                         calendar.notification_args.text = calendar.notification_args.text:gsub("^(.-\n)",
                                                                                                "<span fgcolor='#4e84d5'>%1</span>",1)
                         calendar.notification_args.text = calendar.notification_args.text:gsub("(Su.-)(\n)",
                                                                                                "<span fgcolor='#4eacd5'>%1</span>%2")
                         calendar.notification_args.text = calendar.notification_args.text:gsub("(\n)( ?%d+%s)",
                                                                                                "%1<span fgcolor='#a984cf'>%2</span>")
                         calendar.notification_args.text = calendar.notification_args.text:gsub("(%d+)(\n)",
                                                                                                "<span fgcolor='#a984cf'>%1</span>%2")
                         calendar.notification_args.text = calendar.notification_args.text:gsub("(25[^\n]-)(31)",
                                                                                                "%1<span fgcolor='#a984cf'>%2</span>")
                     end
        })
textclock = awful.widget.textclock("<span fgcolor='#90b3b3'>%a %b %d %H:%M:%S</span>",1)
textclock:connect_signal("mouse::enter", function ()
                                             calendar.update()
                                             calendar.display()
                                         end)
textclock:connect_signal("mouse::leave", function ()
                                             naughty.destroy(calendar.notification)
                                             calendar.month = tonumber(os.date("%m"))
                                             calendar.year  = tonumber(os.date("%Y"))
                                         end)

textclock:buttons(awful.util.table.join(awful.button({ }, 1, function ()
                                            naughty.destroy(calendar.notification)
                                            calendar.update(1)
                                            calendar.display()
                                        end),
                                        awful.button({ }, 3, function ()
                                            naughty.destroy(calendar.notification)
                                            calendar.update(-1)
                                            calendar.display()
                                        end)))

-- CPU
cpuicon     = wibox.widget.imagebox(beautiful.cpuicon)
cpu         = lain.widgets.cpu({
                   timeout  = 1,
                   settings = function()
                       widget:set_markup(cpu_now.usage)
                   end
              })

-- Mem
memicon     = wibox.widget.imagebox(beautiful.memicon)
mem         = lain.widgets.mem({
                  timeout  = 1,
                  settings = function()
                      widget:set_markup(mem_now.usedpercent)
                  end
              })

-- Battery
batteryicon = wibox.widget.imagebox(beautiful.batteryicon)
battery     = lain.widgets.bat({
                  timeout  = 3,
                  settings = function()
                      widget:set_text(bat_now.perc)
                  end
              })


-- Alsa
volumeicon  = wibox.widget.imagebox(beautiful.volumeicon)
volume      = lain.widgets.alsa({
                  timeout  = 5,
                  settings = function()
                      widget:set_text(volume_now.level)
                  end
              })



-- Mail
mailicon             = wibox.widget.imagebox("")
mailspace            = wibox.widget.imagebox("")
maill                = wibox.widget.imagebox("")
mailr                = wibox.widget.imagebox("")

mailwidget, mailicon = mail({ server = "imap.gmail.com", user = ">_>", pass = function() return "<_<" end, -- Put in your own login info.
                                settings = function()
                                    if mail.count > 0 then
                                        mail.icon:set_image(beautiful.mailicon)
                                        mailspace:set_image(beautiful.space)
                                        maill:set_image(beautiful.seperator_right)
                                        mailr:set_image(beautiful.seperator_left)
                                    else
                                        mail.icon:set_image(nil)
                                        mailspace:set_image(nil)
                                        maill:set_image(nil)
                                        mailr:set_image(nil)
                                    end
                                end
                           })
                           
mailwidget:buttons(awful.util.table.join(awful.button({}, 1, function () awful.util.spawn(mutt) end)))
mailicon:buttons(awful.util.table.join(awful.button({}, 1, function () awful.util.spawn(mutt) end)))
maill:buttons(awful.util.table.join(awful.button({}, 1, function () awful.util.spawn(mutt) end)))
mailr:buttons(awful.util.table.join(awful.button({}, 1, function () awful.util.spawn(mutt) end)))
mailspace:buttons(awful.util.table.join(awful.button({}, 1, function () awful.util.spawn(mutt) end)))

-- Net
wifiicon   = wibox.widget.imagebox(beautiful.wifiicon_high)
wifi       = wibox.widget.textbox()
function wifiInfo()
    local wifiStrength = awful.util.pread("awk 'NR==3 {printf \"%1d\\n\",($3/70)*100}' /proc/net/wireless")
    if wifiStrength == "" then
        wifi:set_markup("<span fgcolor='#444444'>--</span>")
        wifiicon:set_image(beautiful.wifiicon_none)
    else
        wifi:set_markup(wifiStrength)
        wifiicon:set_image(beautiful.wifiicon_high)
        if tonumber(wifiStrength) < 67 then wifiicon:set_image(beautiful.wifiicon_med) end
        if tonumber(wifiStrength) < 33 then wifiicon:set_image(beautiful.wifiicon_low) end
    end
end

wifiInfo()
wifi_timer = timer({ timeout = 5 })
wifi_timer:connect_signal("timeout",wifiInfo)
wifi_timer:start()
wifi_timer:emit_signal("timeout")

-- Thermal
thermicon = wibox.widget.imagebox(beautiful.thermicon)
therm     = wibox.widget.textbox()
vicious.register(therm, vicious.widgets.thermal, "$1", 5, "thermal_zone0")

-- Weather
weatherspace = wibox.widget.imagebox(beautiful.space)
weather_force_notify, weather.mouseover = false, false
weatherwidget, weathericon = weather({ station_id = "....", -- You gotta set this one yourself.
                                       settings = function ()
                                           weatherspace:set_image(beautiful.space)
                                           if not currenticon then
                                               weatherspace:set_image(nil)
                                           end
                                           weather.notification_args.text = string.format("<span fgcolor='#4e84d5'>Temperature</span>   %s\n" ..
                                                                                          "<span fgcolor='#4e84d5'>Weather</span>       %s\n" ..
                                                                                          "<span fgcolor='#4e84d5'>Wind</span>          %s - %s\n" ..
                                                                                          "<span fgcolor='#4e84d5'>Humidity</span>      %s\n" ..
                                                                                          "<span fgcolor='#4e84d5'>Pressure</span>      %s\n" ..
                                                                                          "<span fgcolor='#4e84d5'>Dew Point</span>     %s",
                                                                                          prettyprint(weather["temp_c"], " °C"),
                                                                                                      weather["weather"],
                                                                                          prettyprint(weather["wind_kt"], " kt"),
                                                                                          prettyprint(weather["wind_degrees"], "°"),
                                                                                          prettyprint(weather["relative_humidity"], "%"),
                                                                                          prettyprint(weather["pressure_mb"], " mb"),
                                                                                          prettyprint(weather["dewpoint_c"], " °C"))
                                                                                          
                                           weather.disable_notify = true
                                           
                                           if weather_force_notify then
                                               weather.disable_notify = false
                                           end
                                           if not weather.disable_notify then
                                               purgenotifications()
                                           end
                                       end
                                    })

weatherwidget:connect_signal("mouse::enter", function ()
                                             weather.disable_update            = true
                                             weather_mouseover                 = true
                                             weather_force_notify              = true
                                             local _                           = weather.notification_args.timeout
                                             weather.notification_args.timeout = 0
                                             weather.update()
                                             weather.notification_args.timeout = _
                                             weather_force_notify              = false
                                             weather.disable_update            = false
                                         end)

weatherwidget:connect_signal("mouse::leave", function ()
                                             purgenotifications()
                                             weather_mouseover                 = false
                                         end)

weathericon:connect_signal("mouse::enter", function () weatherwidget:emit_signal("mouse::enter") end)
weathericon:connect_signal("mouse::leave", function () weatherwidget:emit_signal("mouse::leave") end)

-- MPD
mpd_force_notify, mpd_mouseover, mpd_firstrun, currentsong, currentstate = false, false, true, "N/A", "N/A"
mpdwidget = mpd({ music_dir             = os.getenv("HOME") .. "/Music", -- Put your music directory here.
                  settings              = function ()
                      if mpd.state == "play" then
                          mpd.widget:set_markup(string.format("<span fgcolor='#826ade'>%s</span> " ..
                                                              "<span fgcolor='#555555'>❭❭</span> " ..
                                                              "<span fgcolor='#4e84d5'>%s</span>",
                                                              mpd.artist, mpd.title))
                          mpd.notification_args.text = string.format("<span fgcolor='#826ade'>%s</span> " ..
                                                                     "<span fgcolor='#555555'>❭❭</span> " ..
                                                                     "<span fgcolor='#4e84d5'>%s</span> " ..
                                                                     "<span fgcolor='#555555'>❭❭</span> " ..
                                                                     "<span fgcolor='#a984cf'>%s</span>\n" ..
                                                                     "<span fgcolor='#4eacd5'>%s</span>",
                                                                     mpd.artist or "N/A", mpd.album or "N/A",
                                                                     mpd.date or "N/A", mpd.title or "N/A")
                      elseif mpd.state == "pause" then
                          mpd.widget:set_markup(string.format("<span fgcolor='#826ade'>%s</span> " ..
                                                              "<span fgcolor='#333333'>❭❭</span> " ..
                                                              "<span fgcolor='#4e84d5'>%s</span>",
                                                              mpd.artist, mpd.title))
                          mpd.notification_args.text = string.format("<span fgcolor='#826ade'>%s</span> " ..
                                                                     "<span fgcolor='#333333'>❭❭</span> " ..
                                                                     "<span fgcolor='#4e84d5'>%s</span> " ..
                                                                     "<span fgcolor='#333333'>❭❭</span> " ..
                                                                     "<span fgcolor='#a984cf'>%s</span>\n" ..
                                                                     "<span fgcolor='#4eacd5'>%s</span>",
                                                                     mpd.artist or "N/A", mpd.album or "N/A",
                                                                     mpd.date or "N/A", mpd.title or "N/A")
                      else
                          mpd.widget:set_markup(string.format("<span fgcolor='#333333'>--</span>"))
                          mpd.notification_args.text = string.format("<span fgcolor='#333333'>%s ❭❭ %s ❭❭ %s\n%s</span>",
                                                                     mpd.artist or "N/A", mpd.album or "N/A",
                                                                     mpd.date or "N/A", mpd.title or "N/A")
                      end

                      statechange                          = false
                      songchange                           = false
                      mpd.disable_notify                   = true
                      mpd.disable_thumbnail_generation     = true
                      
                      if mpd_firstrun then
                          currentsong = mpd.file or "N/A"
                          currentstate = mpd.state or "N/A"
                          if currentsong ~= "N/A" and currentstate ~= "N/A" then
                              mpd_firstrun                     = false
                          end
                      end
                      
                      if currentstate ~= (mpd.state or "N/A") then
                          statechange                      = true
                          currentstate                     = mpd.state
                      end
                      if currentsong  ~= (mpd.file or "N/A") then
                          songchange                       = true
                          currentsong                      = mpd.file
                      end
                      
                      if statechange                  and
                         currentstate == "play"       then
                          mpd.disable_notify               = false
                      end
                      
                      if songchange                   then
                          mpd.disable_thumbnail_generation = false
                          mpd.disable_notify               = false
                      end
                      
                      if mpd_force_notify                 then
                          mpd.disable_notify               = false
                      end
                      
                      if not io.open("/tmp/mpdcover.png") and currentsong ~= "N/A" then
                          mpd.disable_thumbnail_generation = false
                      end
                      
                      if not mpd.disable_notify       then
                          purgenotifications()
                      end
                  end
               })

mpdwidget:connect_signal("mouse::enter", function ()
                                             mpd_mouseover                 = true
                                             mpd_force_notify              = true
                                             local _                       = mpd.notification_args.timeout
                                             mpd.notification_args.timeout = 0
                                             mpd.update()
                                             mpd.notification_args.timeout = _
                                             mpd_force_notify              = false
                                         end)

mpdwidget:connect_signal("mouse::leave", function ()
                                             purgenotifications()
                                             mpd_mouseover                 = false
                                         end)

mpdwidget:buttons(awful.util.table.join(awful.button({}, 1, function ()
                                                                awful.util.spawn_with_shell(" mpc next  ||  ncmpcpp next  ||  ncmpc next  ||  pms next ")
                                                                mpd.update()
                                                            end),
                                        awful.button({}, 2, function ()
                                                                awful.util.spawn_with_shell("mpc toggle || ncmpcpp toggle || ncmpc toggle || pms toggle")
                                                                mpd.update()
                                                            end),
                                        awful.button({}, 3, function ()
                                                                awful.util.spawn_with_shell(" mpc prev  ||  ncmpcpp prev  ||  ncmpc prev  ||  pms prev ")
                                                                mpd.update()
                                                            end)))

-- }}}
