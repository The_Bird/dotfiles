
function run_once(cmd)
  if os.execute("pgrep -cau $USER -f '" .. cmd .. "'") ~= 0 then awful.util.spawn_with_shell(cmd) end
end

awful.util.spawn_with_shell("mpd")
awful.util.spawn_with_shell("compton -b --config /home/thebird/.config/compton/conf")
--run_once("urxvtd -q -o -f")
--xrun("nightly")
run_once("thunar")
run_once("urxvt -name htop -e htop")
run_once("urxvt -name ncmpcpp -e ncmpcpp")
run_once("urxvt -name startterm")
