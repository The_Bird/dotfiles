-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = { -- 򚪩
   --names = {" 򚪪 ", " 򚪫 ", "򚪤򚪬򚪤", 4, 5, 6},
    names = {"򚪪", "򚪫", "򚪬", "򚪭", "򚪮", "򚪯"},
    layout = {awful.layout.layouts[1], awful.layout.layouts[2], awful.layout.layouts[2], awful.layout.layouts[2], awful.layout.layouts[2],
              awful.layout.layouts[2]}
}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tags.names, s, tags.layout)
end
-- awful.tag.incnmaster(1)
-- }}}
