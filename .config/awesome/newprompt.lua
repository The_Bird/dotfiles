---------------------------------------------------------------------------
-- @author Julien Danjou &lt;julien@danjou.info&gt;
-- @copyright 2009 Julien Danjou
-- @release v3.5.2-164-g9a0ba0f
---------------------------------------------------------------------------

local setmetatable = setmetatable

local completion = require("awful.completion")
local util = require("awful.util")
local prompt = require("thing")
local widget_base = require("wibox.widget.base")
local textbox = require("wibox.widget.textbox")
local type = type

-- awful.widget.prompt
local widgetprompt = { mt = {} }

--- Run method for promptbox.
-- @param promptbox The promptbox to run.
local function run(promptbox)
    return prompt.run({ prompt = promptbox.prompt, fg_cursor = "#90b3b3", bg_cursor = "#90b3b3" },
                      promptbox.widget,
                      function (...)
                          local result = util.spawn(...)
                          if type(result) == "string" then
                              promptbox.widget:set_text(result)
                          end
                      end,
                      completion.shell,
                      util.getdir("cache") .. "/history",nil,
                      function (...)
                          promptl:set_image(nil)
                          promptspace:set_image(nil)
                          promptr:set_image(nil)
                      end)
end

--- Create a prompt widget which will launch a command.
-- @param args Arguments table. "prompt" is the prompt to use.
-- @return A launcher widget.
function widgetprompt.new(args)
    local args = args or {}
    local widget = textbox()
    local promptbox = widget_base.make_widget(widget)

    promptbox.widget = widget
    promptbox.widget:set_ellipsize("start")
    promptbox.run = run
    promptbox.prompt = args.prompt or "Run: "
    return promptbox
end

function widgetprompt.mt:__call(...)
    return widgetprompt.new(...)
end

return setmetatable(widgetprompt, widgetprompt.mt)

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
