-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()	
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),
    
    awful.key({ modkey }, "=", function () lain.util.useless_gaps_resize(1) end),
    awful.key({ modkey }, "-", function () lain.util.useless_gaps_resize(-1) end),
    awful.key({ modkey }, "0",
        function ()
            beautiful.useless_gap_width = 8
            awful.layout.arrange(mouse.screen)
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Shift"   }, "Return", function () awful.util.spawn(fterminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey },            "r",     function () 
        promptl:set_image(beautiful.seperator_left)
        promptspace:set_image(beautiful.space)
        promptr:set_image(beautiful.seperator_right)
        beautiful.fg_normal = "#00ff00"
        mypromptbox[mouse.screen]:run()
    end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end),
    
    -- ALSA Mixer
    awful.key({ modkey, "Shift" }, "m",
		function ()
			awful.util.spawn_with_shell("urxvt -name fterminal -title alsamixer -e alsamixer")
		end),
		
	-- Volume Control
    awful.key({ modkey, "Shift" }, "Up",
        function ()
            awful.util.spawn_with_shell("amixer -q set Master 1%+")
            volume.update()
            volume.update()
        end),
    awful.key({ modkey, "Shift" }, "Down",
        function ()
            awful.util.spawn_with_shell("amixer -q set Master 1%-")
            volume.update()
            volume.update()
        end),
        
    -- Weather Control (MUHAHAHAHA)
    awful.key({ modkey, }, "w",
		function ()
			weather.disable_update = true
			weather_force_notify   = true
			weather.update()
			weather_force_notify   = false
			weather.disable_update = false
		end),
    awful.key({ modkey, "Shift"}, "w",
		function ()
			weather.update()
		end),
    
    -- MPD control
    awful.key({ altkey, "Control" }, "Up",
        function ()
            awful.util.spawn_with_shell("mpc toggle || ncmpcpp toggle || ncmpc toggle || pms toggle")
            mpd.update()
        end),
    awful.key({ modkey, "Control" }, "Up",
        function ()
            awful.util.spawn_with_shell("mpc single || ncmpcpp single || ncmpc single || pms single")
            mpd.update()
        end),
    awful.key({ modkey, "Control" }, "Down",
        function ()
            awful.util.spawn_with_shell("mpc random || ncmpcpp random || ncmpc random || pms random")
            mpd.update()
        end),
    awful.key({ altkey, "Control" }, "Down",
        function ()
            awful.util.spawn_with_shell(" mpc stop  ||  ncmpcpp stop  ||  ncmpc stop  ||  pms stop ")
            mpd.update()
        end),
    awful.key({ altkey, "Control" }, "Left",
        function ()
            awful.util.spawn_with_shell(" mpc prev  ||  ncmpcpp prev  ||  ncmpc prev  ||  pms prev ")
            mpd.update()
        end),
    awful.key({ altkey, "Control" }, "Right",
        function ()
            awful.util.spawn_with_shell(" mpc next  ||  ncmpcpp next  ||  ncmpc next  ||  pms next ")
            mpd.update()
        end),
    awful.key({ modkey, }, "d",
        function ()
			mpd_force_notify = true
            mpd.update()
            mpd_force_notify = false
		end),
	-- Mail Control
	awful.key({ modkey, "Shift" }, "l",
		function ()
			mail.update()
		end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = awful.tag.gettags(client.focus.screen)[i]
                          if tag then
                              awful.client.movetotag(tag)
                          end
                     end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}
