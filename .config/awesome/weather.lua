-- Highly customizable weather widget
-- Licensed under GNU General Public License v3

local weather = { mt = {} }

function weather.new(args)
    local args                     = args or {}
    weather.notification           = nil
	
	-- Sets the station id used by the default
	-- weather.gov information source.
    local station_id               = args.station_id
    
    -- Regular expression to be applied to the retrieval command's output.
    -- It requires two captures: one for the data field name and one for
    -- the value of the field.
    local pattern                  = args.pattern or "\n%s-<(.-)>([%S ]-)</%1>%s-"
    
    -- Information needed by the default thumbnail generation function.
    -- This is intended to allow for a large amount of customization so
    -- that the user doesn't need to write their own generation function
    -- in most cases.
    local thumbnail_width          = args.thumbnail_width      or 96
    local thumbnail_height         = args.thumbnail_height     or 96
    local thumbnail_path           = args.thumbnail_path       or ".weather/weatherthumbnail.png"
    local thumbnail_background     = args.thumbnail_background or "transparent"
    local thumbnail_gravity        = args.thumbnail_gravity    or "center"
    
    -- Table containing paths to the icons that should be set for each
    -- kind of weather.
    local icons                    = args.icons or { fair   = beautiful.fair   or (awful.util.getdir("config") .. "/thebird/icons/weather/fair.png"),
                                                     pcloud = beautiful.pcloud or (awful.util.getdir("config") .. "/thebird/icons/weather/pcloud.png"),
                                                     cloud  = beautiful.cloud  or (awful.util.getdir("config") .. "/thebird/icons/weather/cloud.png"),
                                                     rain   = beautiful.rain   or (awful.util.getdir("config") .. "/thebird/icons/weather/rain.png"),
                                                     storm  = beautiful.storm  or (awful.util.getdir("config") .. "/thebird/icons/weather/storm.png"),
                                                     mist   = beautiful.mist   or (awful.util.getdir("config") .. "/thebird/icons/weather/mist.png"),
                                                     snow   = beautiful.snow   or (awful.util.getdir("config") .. "/thebird/icons/weather/snow.png") }
    
    -- Time between data retrievals.
    local timeout                  = args.timeout         or 600
    
    -- Timeout for connecting to the default weather.govinformation source.
    local network_timeout          = args.network_timeout or 3
    
    -- Parts of the default command that will be called to retieve
    -- data from weather.gov.
    local head_command             = args.head_command      or "wget --timeout " .. network_timeout .. " -q --output-document - "
    local path_to_weather          = args.path_to_weather   or "http://w1.weather.gov/xml/current_obs/"
    local weather_extension        = args.weather_extension or ".xml"

    -- Allows the user to define a function that executes
    -- whenever the widget's update function is called.
    -- This can be used to customize the widget and notification
    -- text, contol notification generation, and even to
    -- add functionality to the widget. The default function can
    -- be used as a model for users to design their own function.
    local settings                 = args.settings or function ()
                                                          -- Sets the text and colors of the notification
                                                          weather.notification_args.text = string.format("<span fgcolor='#4e84d5'>Temperature</span>   %s\n" ..
                                                                                                         "<span fgcolor='#4e84d5'>Weather</span>       %s\n" ..
                                                                                                         "<span fgcolor='#4e84d5'>Wind</span>          %s - %s\n" ..
                                                                                                         "<span fgcolor='#4e84d5'>Humidity</span>      %s\n" ..
                                                                                                         "<span fgcolor='#4e84d5'>Pressure</span>      %s\n" ..
                                                                                                         "<span fgcolor='#4e84d5'>Dew Point</span>     %s",
                                                                                                         prettyprint(weather["temp_c"], " °C"),
                                                                                                                     weather["weather"],
                                                                                                         prettyprint(weather["wind_kt"], " kt"),
                                                                                                         prettyprint(weather["wind_degrees"], "°"),
                                                                                                         prettyprint(weather["relative_humidity"], "%"),
                                                                                                         prettyprint(weather["pressure_mb"], " mb"),
                                                                                                         prettyprint(weather["dewpoint_c"], " °C"))
                                                          -- Resets variable that causes generation
                                                          -- of the notification.
                                                          weather.disable_notify = true
                                                          
                                                          -- The weather_force_notify variable would be set by the user
                                                          -- to force the notification to appear (for example, on a
                                                          -- keypress) should the user decide to define their own
                                                          -- settings() function (in this default function there is
                                                          -- nothing that would cause weather_force_notify to be true,
                                                          -- so this merely serves to illustrate a possible extension
                                                          -- of functionality by the user).
                                                          if weather_force_notify then
                                                              weather.disable_notify = false
                                                          end
                                                          
                                                          -- This code snippet calls a function that destroys all other
                                                          -- visible notifications if this widget is about to generate its
                                                          -- own. It is commented out because the function purgenotifications()
                                                          -- needs to be defined by the user since it would neccesarily depend
                                                          -- on having references to the notification objects that are to be
                                                          -- destroyed (weather.notification provides one for this widget). It is 
                                                          -- just to serve as an example for a user to use in their own settings
                                                          -- function.
                                                          -- -- if not weather.disable_notify then
                                                          -- --     purgenotifications()
                                                          -- -- end
                                                      end

    
    weather.widget                 = wibox.widget.textbox('')
    weather.icon                   = wibox.widget.imagebox('')
    
    weather.disable_update               = false
    weather.disable_notify               = true
    weather.disable_thumbnail_generation = false
    weather.notification_args            = { timeout  = 4,
                                             position = "bottom_right",
                                             icon     = ".weather/weatherthumbnail.png" }
    
    -- Internal function that formats the data that is used
    -- by the default notification and resolves certain quirks
    -- that result from the weather.gov formats.
    function prettyprint(x, suffix)
        if tonumber(x) == 999 then return "Variable" end
        if tonumber(x) then
            local _=0
            if tonumber(x)~=0 then
                _ = math.abs(tonumber(x))/tonumber(x)
            end
            return math.floor(tonumber(x)+_/2) .. (suffix or "")
        else
            return "N/A"
        end
    end
    
    -- Allows the user to define the function that sets the icon.
    -- Can be used in conjunction with a custom fetch command
    -- to allow the widget to function with sources other than
    -- weather.gov.
    local seticon = args.seticon or function()
        if not weather["weather"] then
            weather["weather"] = ""
            currenticon=nil
        end
        if string.find(weather["weather"],"Fair")          ~= nil or
           string.find(weather["weather"],"A Few Clouds")  ~= nil then
            currenticon=icons.fair
        end
        if string.find(weather["weather"],"Partly Cloudy") ~= nil then
            currenticon=icons.pcloud
        end
        if string.find(weather["weather"],"Fog")           ~= nil or
           string.find(weather["weather"],"Mist")          ~= nil or
           string.find(weather["weather"],"Haze")          ~= nil then
            currenticon=icons.mist
        end
        if string.find(weather["weather"],"Overcast")      ~= nil or
           string.find(weather["weather"],"Mostly Cloudy") ~= nil or
           string.find(weather["weather"],"Sky Obscured")  ~= nil then
            currenticon=icons.cloud
        end
        if string.find(weather["weather"],"Snow")          ~= nil or
           string.find(weather["weather"],"Hail")          ~= nil or
           string.find(weather["weather"],"Mixed")         ~= nil or
           string.find(weather["weather"],"Sleet")         ~= nil then
            currenticon=icons.snow
        end
        if string.find(weather["weather"],"Rain")          ~= nil or
            string.find(weather["weather"],"Drizzle")      ~= nil then
            currenticon=icons.rain
        end
        if string.find(weather["weather"],"Thunderstorm")  ~= nil then
            currenticon=icons.storm
        end
    end
    
    -- Allows the user to define the function that generates
    -- the thumbnail. Can be used in conjunction with a custom
    -- fetch command to allow the widget to function with sources
    -- other than weather.gov.
    local generatethumbnail = args.generatethumbnail or function()
        weatherthumbnailurl = (weather["icon_url_base"] or "") .. (weather["icon_url_name"] or "")
        os.execute('wget --output-document "' .. thumbnail_path ..'" "' .. weatherthumbnailurl .. '"')
        os.execute('convert "' .. thumbnail_path ..
                   '" -thumbnail "' .. thumbnail_width .. 'x' .. thumbnail_height ..
                   '" -gravity "' .. thumbnail_gravity ..
                   '" -background "' .. thumbnail_background ..
                   '" -extent "' .. thumbnail_width .. 'x' .. thumbnail_height ..
                   '" "' .. thumbnail_path .. '"')
    end
    
    function weather.update()
        if not weather.disable_update then
            local fetch_command = args.fetch_command or head_command .. path_to_weather .. station_id .. weather_extension
            local f = io.popen(fetch_command)
            for tag, value in f:read("*a"):gmatch(pattern) do
                weather[tag] = value
            end
            f:close()
            generatethumbnail()
            seticon()
        end
        weather.disable_notify               = true
        weather.widget:set_text(string.format("%s", prettyprint(weather["temp_c"])))
        weather.icon:set_image(currenticon)
        weather.notification_args.text = string.format("Temperature: %s\nWeather: %s\nWind: %s - %s\nHumidity: %s\nPressure: %s\nDew Point: %s",
                                                     prettyprint(weather["temp_c"], " °C"),
                                                                 weather["weather"],
                                                     prettyprint(weather["wind_kt"], " kt"),
                                                     prettyprint(weather["wind_degrees"], "°"),
                                                     prettyprint(weather["relative_humidity"], "%"),
                                                     prettyprint(weather["pressure_mb"], " mb"),
                                                     prettyprint(weather["dewpoint_c"], " °C"))
        settings()
        if not weather.disable_notify then weather.notification = naughty.notify(weather.notification_args) end
    end
    
    -- Creates a timer object that updates the widget
    weather.timer = timer({ timeout = timeout })
    weather.timer:connect_signal("timeout", weather.update)
    weather.timer:start()
    weather.timer:emit_signal("timeout")
    
    return weather.widget, weather.icon
end

function weather.mt:__call(...)
    return weather.new(...)
end

return setmetatable(weather,weather.mt)
