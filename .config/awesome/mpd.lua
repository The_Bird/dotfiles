-- Highly customizable MPD widget
-- Licensed under GNU General Public License v3

local mpd = { mt = {} }

function mpd.new(args)
    local args                       = args or {}
    mpd.notification                 = nil
    
    -- Information needed to connect to the MPD server and retrieve
    -- the state data using the default retrieval command. Can be set
    -- by the user so that they don't need to re-write the command
    -- if they just have a non-standard server host and port and/or
    -- have a password on the MPD server.
    local pass                       = args.pass or args.password or ""
    local host                       = args.host or "localhost"
    local port                       = args.port or "6600"
    
    -- Information needed by the default thumbnail generation function.
    -- This is intended to allow for a large amount of customization so
    -- that the user doesn't need to write their own generation function
    -- in most cases.
    local music_dir                  = args.music_dir or os.getenv("HOME") .. "/Music";
    local thumbnail_width            = args.thumbnail_width or 96
    local thumbnail_height           = args.thumbnail_height or 96
    local thumbnail_gravity          = args.thumbnail_gravity or "center"
    local thumbnail_background       = args.thumbnail_background or "transparent"
    
    -- Sets the time between updates. The update function can also
    -- be called by the user directly whenever a forced update is
    -- needed.
    local timeout                    = args.timeout or 1
    
    -- Command to be executed by the OS to output MPD's state data.
    -- Default command was stolen from https://github.com/copycat-killer/lain
    -- because I didn't feel like figuring out how MPD worked.
    local mpd_retrieval_command      = args.mpd_retrieval_command or "echo 'password " .. pass .. "\nstatus\ncurrentsong\nclose'" ..
                                                                     " | curl --connect-timeout 1 -fsm 3 " ..
                                                                     "telnet://" .. host .. ":" .. port
    
    -- Regular expression to be applied to the retrieval command's output.
    -- It requires two captures: one for the data field name and one for
    -- the value of the field.
    local pattern                    = args.pattern or "(%S-): (.-)\n"
    
    -- Allows the user to provide their own function for
    -- thumbnail generation. The default function is adapted
    -- from a bash script that handled thumbnail generation
    -- for Lain's MPD widget: https://github.com/copycat-killer/lain
    local thumbnail_generate         = args.thumbnail_generate or function () 
                                                                      if string.sub(music_dir,-1) ~= "/" then music_dir = music_dir .. "/" end

                                                                      local search_pattern   = "(Front|front|Cover|cover|Art|art|Folder|folder)" ..
                                                                                               "\.(jpg|jpeg|png|gif)$"
                                                                      local fullpath         = music_dir .. mpd.file
                                                                      local current_dir      = string.sub(fullpath, 1,
                                                                                               string.find(fullpath, "/[^/]+$"))
                                                                      local _                = io.popen('find \"' .. current_dir ..
                                                                                               '\" -maxdepth 1 -type f | egrep -i -m1 \"' ..
                                                                                               search_pattern .. '\"')
                                                                      local cover_image_path = _:read() or ""
                                                                      os.execute('convert "'       .. cover_image_path     ..
                                                                                 '" -thumbnail "'  .. thumbnail_width      .. 'x' .. thumbnail_height ..
                                                                                 '" -gravity "'    .. thumbnail_gravity    ..
                                                                                 '" -background "' .. thumbnail_background ..
                                                                                 '" -extent "'     .. thumbnail_width      .. 'x' .. thumbnail_height ..
                                                                                 '" "' .. mpd.notification_args.icon .. '"')
                                                                  end
    
    -- Allows the user to change the colors used by the default
    -- settings function, so that they don't need to re-write the
    -- function just to make simple color customizations. The user
    -- can set the color of each individual part of the default widget
    -- and may optionally set different colors for when MPD is
    -- paused or stopped.
    local song_color              = args.song_color              or beautiful.fg_normal    or "#ffffff"
    local seperator_color         = args.seperator_color         or beautiful.fg_normal    or "#ffffff"
    local title_color             = args.title_color             or beautiful.fg_normal    or "#ffffff"
    
    local song_color_playing      = args.song_color_playing      or song_color
    local seperator_color_playing = args.seperator_color_playing or seperator_color
    local title_color_playing     = args.title_color_playing     or title_color
    
    local song_color_paused       = args.song_color_paused       or song_color
    local seperator_color_paused  = args.seperator_color_paused  or seperator_color
    local title_color_paused      = args.title_color_paused      or title_color
    
    local color_stopped           = args.color_stopped           or beautiful.fg_minimize  or "#666666"
    
    -- These are state variables used by the default settings function to check
    -- for song or state changes. Variables like these could be very
    -- useful to users seeking to implement their own settings function
    -- as the information they provide allows for quite a bit of functionality,
    -- as will be demonstrated below. The underscore (as opposed to a dot) between 
    -- "mpd" and the rest of the variable name for two of these variables is not a 
    -- mistake - these variables are not part of the widget itself and are imagined to be set by
    -- the user (perhaps with better names) in the same scope as the widget initialization
    -- (i.e. in the rc.lua) should the user decude to make their own settings function.
    local mpd_force_notify, mpd_firstrun, currentsong, currentstate = false, true, "N/A", "N/A"
    
    -- Allows the user to define a function that executes
    -- whenever the widget's update function is called.
    -- This can be used to customize the widget and notification
    -- text, contol thumbnail and notification generation, and
    -- even to add functionality to the widget. The default function
    -- (along with some equivilent to the state variables above)
    -- can be used as a model for users to design their own function.
    local settings = args.settings or function ()
                                          -- Sets the text and colors of both the widget and the notification
                                          -- depending on the current state of MPD.
                                          if mpd.state == "play" then
                                              mpd.widget:set_markup(string.format("<span fgcolor='" .. song_color_playing      .. "'>%s</span> " ..
                                                                                  "<span fgcolor='" .. seperator_color_playing .. "'>>></span> " ..
                                                                                  "<span fgcolor='" .. title_color_playing     .. "'>%s</span>",
                                                                                  mpd.artist, mpd.title))
                                              mpd.notification_args.text = string.format("<span fgcolor='" .. song_color_playing      .. "'>%s</span> " ..
                                                                                         "<span fgcolor='" .. seperator_color_playing .. "'>>></span> " ..
                                                                                         "<span fgcolor='" .. song_color_playing      .. "'>%s</span> " ..
                                                                                         "<span fgcolor='" .. seperator_color_playing .. "'>>></span> " ..
                                                                                         "<span fgcolor='" .. song_color_playing      .. "'>%s</span>\n" ..
                                                                                         "<span fgcolor='" .. title_color_playing     .. "'>%s</span>",
                                                                                         mpd.artist or "N/A", mpd.album or "N/A",
                                                                                         mpd.date or "N/A", mpd.title or "N/A")
                                          elseif mpd.state == "pause" then
                                              mpd.widget:set_markup(string.format("<span fgcolor='" .. song_color_paused      .. "'>%s</span> " ..
                                                                                  "<span fgcolor='" .. seperator_color_paused .. "'>>></span> " ..
                                                                                  "<span fgcolor='" .. title_color_paused     .. "'>%s</span>",
                                                                                  mpd.artist, mpd.title))
                                              mpd.notification_args.text = string.format("<span fgcolor='" .. song_color_paused      .. "'>%s</span> " ..
                                                                                         "<span fgcolor='" .. seperator_color_paused .. "'>>></span> " ..
                                                                                         "<span fgcolor='" .. song_color_paused      .. "'>%s</span> " ..
                                                                                         "<span fgcolor='" .. seperator_color_paused .. "'>>></span> " ..
                                                                                         "<span fgcolor='" .. song_color_paused      .. "'>%s</span>\n" ..
                                                                                         "<span fgcolor='" .. title_color_paused     .. "'>%s</span>",
                                                                                         mpd.artist or "N/A", mpd.album or "N/A",
                                                                                         mpd.date or "N/A", mpd.title or "N/A")
                                          else
                                              mpd.widget:set_markup(string.format("<span fgcolor='" .. color_stopped .. "'>--</span>"))
                                              mpd.notification_args.text = string.format("<span fgcolor='" .. color_stopped .. "'>%s >> %s >> %s\n%s</span>",
                                                                                         mpd.artist or "N/A", mpd.album or "N/A",
                                                                                         mpd.date or "N/A", mpd.title or "N/A")
                                          end
                                          
                                          -- Resets variables that indicate a change
                                          -- in song or state.
                                          local statechange                    = false
                                          local songchange                     = false
                                          
                                          -- Resets variables that cause generation
                                          -- of the thumbnail and notification
                                          mpd.disable_notify                   = true
                                          mpd.disable_thumbnail_generation     = true
                                          
                                          -- Prevent notification from appearing when
                                          -- the WM is first started.
                                          if mpd_firstrun then
                                              currentsong = mpd.file or "N/A"
                                              currentstate = mpd.state or "N/A"
                                              if currentsong ~= "N/A" and currentstate ~= "N/A" then
                                                  mpd_firstrun                     = false
                                              end
                                          end
                                          
                                          -- Checks if the state has changed since last update 
                                          -- and if so sets a variable indicating that it has.
                                          if currentstate ~= (mpd.state or "N/A") then
                                              statechange                      = true
                                              currentstate                     = mpd.state
                                          end
                                          
                                          -- Checks if the song has changed since last update 
                                          -- and if so sets a variable indicating that it has.
                                          if currentsong  ~= (mpd.file or "N/A") then
                                              songchange                       = true
                                              currentsong                      = mpd.file
                                          end
                                          
                                          -- Allows the notification to appear if MPD has
                                          -- started playing a song while it was previously
                                          -- stopped or paused.
                                          if statechange                  and
                                             currentstate == "play"       then
                                              mpd.disable_notify               = false
                                          end
                                          
                                          -- Allows the notification to appear and the thumbnail
                                          -- to update if the song that MPD is playing has changed 
                                          -- since the last update.
                                          if songchange                   then
                                              mpd.disable_thumbnail_generation = false
                                              mpd.disable_notify               = false
                                          end
                                          
                                          -- The mpd_force_notify variable would be set by the user
                                          -- to force the notification to appear (for example, on a
                                          -- keypress) should the user decide to define their own
                                          -- settings() function (in this default function there is
                                          -- nothing that would cause mpd_force_notify to be true,
                                          -- so this merely serves to illustrate a possible extension
                                          -- of functionality by the user).
                                          if mpd_force_notify                 then
                                              mpd.disable_notify               = false
                                          end
                                          
                                          -- Forces a thumbnail to be generated if no thumbnail file exists
                                          if not io.open("/tmp/mpdcover.png") and currentsong ~= "N/A" then
                                              mpd.disable_thumbnail_generation = false
                                          end
                                          
                                          -- This code snippet calls a function that destroys all other
                                          -- visible notifications if this widget is about to generate its
                                          -- own. It is commented out because the function purgenotifications()
                                          -- needs to be defined by the user since it would neccesarily depend
                                          -- on having references to the notification objects that are to be
                                          -- destroyed (mpd.notification provides one for this widget). It is 
                                          -- just to serve as an example for a user to use in their own settings
                                          -- function.
                                          -- -- if not mpd.disable_notify       then
                                          -- --     purgenotifications()
                                          -- -- end
                                      end
    
    -- Boolean variables that control the generation of the
    -- notification and the thumbnail. They are intended to be
    -- controlled by the user via the settings() function.
    mpd.disable_notify               = true
    mpd.disable_thumbnail_generation = true
    
    -- Creates the text widget
    mpd.widget                       = wibox.widget.textbox("")
    
    -- The default args that will be used in a call to
    -- naughty.notify to create the notificaiton box. Can
    -- be customized by the user via the settings() function.
    mpd.notification_args            = { timeout         = 2,
                                         position        = "bottom_right",
                                         icon            = "/tmp/mpdcover.png" }
    
    -- Function that updates the widget and possibly generates a
    -- thumbnail and/or a notification box. By default, it is only
    -- called by the timer, but can be called by the user to force
    -- an update at any time (for example, if the user has a key
    -- bound to make MPD play the next song and they want the
    -- widget to update immediatly after to reflect the song change).
    function mpd.update()
        -- Retrieves MPD state data and populates the mpd object
        -- with key and value pairs that correspond to each
        -- data field. Also contains a substitution that escapes
        -- ampersand characters so that they don't get interpreted
        -- as control patterns in the markup.
        local f = io.popen(mpd_retrieval_command)
        for tag, value in f:read("*a"):gmatch(pattern) do
            value=value:gsub([[&]],[[&amp;]])
            mpd[tag]               = value
            mpd[string.lower(tag)] = value
        end
        f:close()
        
        -- Sets the default markup for the widget and notification.
        -- Modified versions of these statements can be used in settings() for customization,
        -- as demonstrated in the default settings function. These are mostly just here as
        -- fallbacks in case something goes terribly wrong. 
        mpd.widget:set_text(string.format("%s - %s", mpd.artist or "N/A", mpd.title or "N/A"))
        mpd.notification_args.text = string.format("%s (%s) - %s\n%s",
                                                   mpd.artist or "N/A", mpd.album or "N/A",
                                                   mpd.date or "N/A", mpd.title or "N/A")
                                                   
        -- Calls user-defined settings function, which is used to modify the widget and
        -- notification markup and decide whether or not a thumbnail should be generated
        -- or if the nofitication should appear (by setting the value of mpd.disable_notify
        -- and/or mpd.disable_thumbnail_generation.
        settings()
        
        -- Generates the thumbnail and/or the notification if the user specified
        -- that the widget should do so.
        if not mpd.disable_thumbnail_generation then thumbnail_generate()                                     end
        if not mpd.disable_notify               then mpd.notification = naughty.notify(mpd.notification_args) end
    end
    
    -- Creates a timer object that checks to see if the song has changed.
    -- This calls mpd.update (which in turn calls settings()) so that the
    -- user has an opportunity to potentially make the widget update and/or
    -- (for example) have the notification pop up on song change
    mpd.timer = timer({ timeout = timeout })
    mpd.timer:connect_signal("timeout", mpd.update)
    mpd.timer:start()
    mpd.timer:emit_signal("timeout")
    
    return mpd.widget
end

function mpd.mt:__call(...)
    return mpd.new(...)
end

return setmetatable(mpd,mpd.mt)
