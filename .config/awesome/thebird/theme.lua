-------------------------------
--  "thebird" awesome theme  --
--        By: TheBird        --
-------------------------------

-- BASICS
theme = {}
theme.font = "TheBirdTags 8"

theme.bg_normal     = "#252525"
theme.bg_focus      = "#252525"
theme.bg_urgent     = "#252525"
theme.bg_minimize   = "#000000"

theme.fg_normal     = "#90b3b3"
theme.fg_focus      = "#4eacd5"
theme.fg_urgent     = "#826ade"
theme.fg_minimize   = "#006699"

theme.border_width  = "0"
theme.border_normal = "#4e84d5"
theme.border_focus  = "#4e84d5"
theme.border_marked = "#8b0000"

theme.useless_gap_width = "8"

theme.tasklist_disable_icon         = true
theme.tasklist_floating             = "✈ "
theme.tasklist_ontop                = "⬆ "
theme.tasklist_sticky               = "◉ "
theme.tasklist_maximized_horizontal = ""
theme.tasklist_maximized_vertical   = "▨ "

-- WIDGET ICONS
theme.seperator       = awful.util.getdir("config") .. "/thebird/icons/seperator.png"
theme.seperator_left  = awful.util.getdir("config") .. "/thebird/icons/seperator_l.png"
theme.seperator_right = awful.util.getdir("config") .. "/thebird/icons/seperator_r.png"
theme.blackgrey       = awful.util.getdir("config") .. "/thebird/icons/blackgrey.png"
theme.greyblack       = awful.util.getdir("config") .. "/thebird/icons/greyblack.png"
theme.space           = awful.util.getdir("config") .. "/thebird/icons/space.png"
theme.greyspace       = awful.util.getdir("config") .. "/thebird/icons/greyspace.png"
theme.batteryicon     = awful.util.getdir("config") .. "/thebird/icons/battery.png"
theme.cpuicon         = awful.util.getdir("config") .. "/thebird/icons/cpu.png"
theme.memicon         = awful.util.getdir("config") .. "/thebird/icons/mem.png"
theme.wifiicon_high   = awful.util.getdir("config") .. "/thebird/icons/wifi_high.png"
theme.wifiicon_med    = awful.util.getdir("config") .. "/thebird/icons/wifi_med.png"
theme.wifiicon_low    = awful.util.getdir("config") .. "/thebird/icons/wifi_low.png"
theme.wifiicon_none   = awful.util.getdir("config") .. "/thebird/icons/wifi_none.png"
theme.volumeicon      = awful.util.getdir("config") .. "/thebird/icons/volume.png"
theme.thermicon       = awful.util.getdir("config") .. "/thebird/icons/therm.png"
theme.mailicon        = awful.util.getdir("config") .. "/thebird/icons/mail.png"

theme.fair   = awful.util.getdir("config") .. "/thebird/icons/weather/fair.png"
theme.pcloud = awful.util.getdir("config") .. "/thebird/icons/weather/pcloud.png"
theme.cloud  = awful.util.getdir("config") .. "/thebird/icons/weather/cloud.png"
theme.rain   = awful.util.getdir("config") .. "/thebird/icons/weather/rain.png"
theme.storm  = awful.util.getdir("config") .. "/thebird/icons/weather/storm.png"
theme.mist   = awful.util.getdir("config") .. "/thebird/icons/weather/mist.png"
theme.snow   = awful.util.getdir("config") .. "/thebird/icons/weather/snow.png"

-- IMAGES
theme.layout_floating        = "/home/thebird/.config/awesome/thebird/icons/floating.png"
theme.layout_fullscreen      = "/home/thebird/.config/awesome/thebird/icons/fullscreen.png"
theme.layout_centerwork      = "/home/thebird/.config/awesome/thebird/icons/center.png"
theme.layout_termfair        = "/home/thebird/.config/awesome/thebird/icons/termfair.png"
theme.layout_uselessfair     = "/home/thebird/.config/awesome/thebird/icons/uselessfair.png"

-- from default for now...
theme.menu_submenu_icon     = "/usr/share/awesome/themes/default/submenu.png"
theme.taglist_squares_sel   = awful.util.getdir("config") .. "/thebird/icons/null.png"
theme.taglist_squares_unsel = awful.util.getdir("config") .. "/thebird/icons/null.png"

-- MISC
theme.wallpaper             = "/home/thebird/.config/awesome/thebird/stripes.png"
theme.taglist_squares       = "false"

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
