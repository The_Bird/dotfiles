local mail = { mt = {} }

function mail.new(args)
    local args            = args or {}
    mail.notification     = nil
    
    local server          = args.server
    local user            = args.user
    local pass            = args.pass
    local port            = args.port or 993
    local network_timeout = args.network_timeout or 3
    local task_timeout    = args.task_timeout or 5
    local timeout         = args.timeout or 60
    local uri             = args.server_uri or "imaps://" .. server .. ":" .. port .. "/INBOX"
    local head_command    = args.head_command or "curl --connect-timeout " .. network_timeout .. " -s -f -m "  .. task_timeout
    local request         = args.request or "-X 'SEARCH UNSEEN'"
    local fetch_command   = fetch_command or head_command .. " --url " .. uri .. " -u " .. user .. ":" .. pass() .. " -kv " .. request
    local settings        = args.settings or function() end
    
    mail.widget = wibox.widget.textbox('')
    mail.icon   = wibox.widget.imagebox('')
    mail.count  = 0
    
    function mail.update()
        mail.count = 0
        for i in io.popen(fetch_command):lines() do
            if i:match("\* SEARCH %d+") then
                for j in i:gmatch(" %d+") do
                    mail.count = mail.count + 1
                end
            end
        end
        mail.widget:set_text('')
        if mail.count > 0 then
            mail.widget:set_text(mail.count)
        end
        settings()
    end
    
    mail.timer = timer({ timeout = timeout })
    mail.timer:connect_signal("timeout", mail.update)
    mail.timer:start()
    mail.timer:emit_signal("timeout")
    
    return mail.widget, mail.icon
end

function mail.mt:__call(...)
    return mail.new(...)
end

return setmetatable(mail,mail.mt)
